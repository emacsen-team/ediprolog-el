;; Generated package description from ediprolog.el  -*- no-byte-compile: t -*-
(define-package "ediprolog" "2.3" "Emacs Does Interactive Prolog" 'nil :commit "ea8710335eec483b576c7a800c92b8fd214aa6dd" :authors '(("Markus Triska" . "triska@metalevel.at")) :maintainer '("Markus Triska" . "triska@metalevel.at") :keywords '("languages" "processes") :url "https://www.metalevel.at/ediprolog/")
